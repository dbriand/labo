#!/bin/bash

#
# Dynamic inventory : take all production environment vars from gitlab-ci and 
# generate an ansible dynamic inventory
# 

##### Env vars
environment_file="$(readlink -f $(dirname $0))/${CI_ENVIRONMENT_NAME:-dev}.yml"

##### Functions

function usage() {
	cat <<EOF
USAGE: $0 (--host host|--list)
	$0 is based on CI_ENVIRONMENT_NAME environment variable to select environment yaml.
EOF
}


function generate_yaml () {
	eval "cat <<EOF
$(<$environment_file)
EOF
"
}


function get_list () {
	generate_yaml | niet -f json .groups
}

function get_host () {
	generate_yaml | niet -f json .hosts.$1
}

##### Main
case $1 in 
	--list)
		get_list
	;;
	--host)
		if [ "$#" -ne 2 ]; then
			echo "ERROR: Argument missing !"
			usage
			exit 2
		fi
		get_host $2
	;;
	*)
		usage
		exit 1
	;;
esac

