```
[root@h2g2w docker]# docker build -t fortuneperso
"docker build" requires exactly 1 argument(s).
See 'docker build --help'.

Usage:  docker build [OPTIONS] PATH | URL | -

Build an image from a Dockerfile
[root@h2g2w docker]# docker build -t fortuneperso .
Sending build context to Docker daemon  80.9 kB
Step 1/4 : FROM docker/whalesay:latest
 ---> 6b362a9f73eb
Step 2/4 : COPY fortune.perso* /usr/share/games/fortunes/
 ---> 689042448fb0
Removing intermediate container 0036d69283b3
Step 3/4 : RUN apt-get -y update && apt-get install -y fortunes
 ---> Running in 830386814a7c
Ign http://archive.ubuntu.com trusty InRelease
Get:1 http://archive.ubuntu.com trusty-updates InRelease [65.9 kB]
Get:2 http://archive.ubuntu.com trusty-security InRelease [65.9 kB]
[...]
Fetched 4647 kB in 7s (646 kB/s)
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following extra packages will be installed:

Selecting previously unselected package fortunes.                                                                                                                                                                                
Preparing to unpack .../fortunes_1%3a1.99.1-7_all.deb ...                                                                                                                                                                        
Unpacking fortunes (1:1.99.1-7) ...                                                                                                                                                                                              
Setting up librecode0:amd64 (3.6-21) ...
Setting up fortune-mod (1:1.99.1-7) ...
Setting up fortunes-min (1:1.99.1-7) ...
Setting up fortunes (1:1.99.1-7) ...
Processing triggers for libc-bin (2.19-0ubuntu6.6) ...
 ---> fba3b768551e
Removing intermediate container 830386814a7c
Step 4/4 : CMD /usr/games/fortune /usr/share/games/fortunes/fortune.perso.txt | cowsay
 ---> Running in 006899d3c253
 ---> 1675e19049e7
Removing intermediate container 006899d3c253
Successfully built 1675e19049e7
[root@h2g2w docker]# docker run -it fortuneperso
 _________________________________________
/ Comme toute science, la mathÃ©matique   \
| ne peut Ãªtre construite sur la seule   |
\ logique.                                /
 -----------------------------------------
    \
     \
      \
                    ##        .
              ## ## ##       ==
           ## ## ## ##      ===
       /""""""""""""""""___/ ===
  ~~~{~~~~~~~~~~~~~~~~~~~~~~~/===-~~~
       \______ o          __/
        \    \        __/
          \____\______/
[root@h2g2w docker]#
``` 
