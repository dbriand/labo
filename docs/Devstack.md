git clone https://git.openstack.org/openstack-dev/devstack
cd devstack
cp samples/local.conf local.conf
vim local.conf
./stack.sh

# here we got an mysql issue on service connection failure
cd /etc/mysql/
cd mysql.conf.d/
vim mysqld.cnf  = no changes but information to copy file :
cp mysqld.cnf  ../conf.d/
./debian-start
sudo mysqladmin -u root -p password
mysqladmin -u root -p create testd