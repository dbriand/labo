## Session 2018 10 24  (october)

Denis.B presents a docker session for Francois, Nicolas, Boubacar, Yannick & Mael from low technical skills.

We start from very minimum knowledge about docker.

first step : 
```
root@squadlabo:~# docker run debian  echo helloworld
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
bc9ab73e5b14: Pull complete
Digest: sha256:802706fa62e75c96fff96ada0e8ca11f570895ae2e9ba4a9d409981750ca544c
Status: Downloaded newer image for debian:latest
helloworld
root@squadlabo:~#
root@squadlabo:~# docker run debian  echo -e "helloworld\nhelloworld\n"
helloworld
helloworld

root@squadlabo:~# 
```

Examples here come from the team server of course 

The presentation file will be provided soon 



```
root@squadlabo:~# docker run debian  echo helloworld
Unable to find image 'debian:latest' locally
latest: Pulling from library/debian
bc9ab73e5b14: Pull complete
Digest: sha256:802706fa62e75c96fff96ada0e8ca11f570895ae2e9ba4a9d409981750ca544c
Status: Downloaded newer image for debian:latest
helloworld
root@squadlabo:~#
root@squadlabo:~# docker run debian  echo -e "helloworld\nhelloworld\n"
helloworld
helloworld

root@squadlabo:~# 
```

One of us is using the team server, other members use their own servers or laptops.

```
root@squadlabo:~# docker run -a stdin -a stdout -i -t ubuntu /bin/bash
Unable to find image 'ubuntu:latest' locally
latest: Pulling from library/ubuntu
473ede7ed136: Pull complete
c46b5fa4d940: Pull complete
93ae3df89c92: Pull complete
6b1eed27cade: Pull complete
Digest: sha256:29934af957c53004d7fb6340139880d23fb1952505a15d69a03af0d1418878cb
Status: Downloaded newer image for ubuntu:latest
root@cd489c710d22:/# df
Filesystem     1K-blocks    Used Available Use% Mounted on
overlay         40182136 3751244  34366664  10% /
tmpfs           65991604       0  65991604   0% /dev
tmpfs           65991604       0  65991604   0% /sys/fs/cgroup
/dev/md2        40182136 3751244  34366664  10% /etc/hosts
shm                65536       0     65536   0% /dev/shm
tmpfs           65991604       0  65991604   0% /sys/firmware
root@cd489c710d22:/# hostname
cd489c710d22
root@cd489c710d22:/# exit
exit
root@squadlabo:~#
```

Here we are for very very starters presentation, session support file will be provided soon.



--- then pizza pause ---



How to create and share a directory to the docker contener 




```
root@squadlabo:~#
root@squadlabo:~# docker run -v /tmp/:/home/toto -it debian /bin/bash
root@2642fc8b830e:/# cd /home/toto
root@2642fc8b830e:/home/toto# ls
ansible_rUWNYh  mc-adminlabo                                                                       ubuntu-release-upgrader-79829d9d
cpu_stats       systemd-private-494e4c3f67354a4f91e5d9031857c48c-systemd-timesyncd.service-q1dJDw  ubuntu-release-upgrader-gg_2y_tr
fileB6UmSI      tmux-0
fileCmkCDa      tmux-1000
root@2642fc8b830e:/home/toto# touch TOTO
root@2642fc8b830e:/home/toto# exit
root@squadlabo:~# ls -l /tmp/TOTO
-rw-r--r-- 1 root root 0 oct.  24 20:55 /tmp/TOTO
root@squadlabo:~#

```

with a sharing with bind method

```
root@squadlabo:~# docker run -v ngnix:/etc/ngnix -it debian /bin/bash
root@35badf02538d:/# cd /etc
root@35badf02538d:/etc# cd ngnix/
root@35badf02538d:/etc/ngnix# ls
root@35badf02538d:/etc/ngnix# touch TITI 
root@35badf02538d:/etc/ngnix#
root@35badf02538d:/etc/ngnix# exit
exit
root@squadlabo:~#
root@squadlabo:~# cd /var/lib/
root@squadlabo:~# cd /var/lib/docker/
root@squadlabo:~# cd /var/lib/docker/volumes/ngnix/
root@squadlabo:/var/lib/docker/volumes/ngnix# cd _data/
root@squadlabo:/var/lib/docker/volumes/ngnix/_data# ls
TITI
root@squadlabo:/var/lib/docker/volumes/ngnix/_data#
```

look at volume after 

```
root@squadlabo:~# docker inspect ngnix
[
    {
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/ngnix/_data",
        "Name": "ngnix",
        "Options": {},
        "Scope": "local"
    }
]
root@squadlabo:~#
```   

Create a container from a docker file with a very simple example :

```
root@squadlabo:/home/Sandbox/Docker# cat Dockerfile
FROM alpine:latest

MAINTAINER dbriand@squad.fr

ADD fichierexample /etc/fichierexample

ADD run.sh /usr/local/bin/run.sh

RUN chmod 755 /usr/local/bin/run.sh

ENTRYPOINT ["/usr/local/bin/run.sh", "-c", "argument"]
root@squadlabo:/home/Sandbox/Docker#
```

modify it for a test of an echo

```
root@squadlabo:/home/Sandbox/Docker# cat docker-entrypoint.sh
#!/bin/bash

echo $1

exit0
root@squadlabo:/home/Sandbox/Docker# cat Dockerfile
FROM alpine:latest

MAINTAINER dbriand@squad.fr

COPY docker-entrypoint.sh /usr/local/bin/

RUN chmod 755 /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]

CMD ["fake postgres"]
root@squadlabo:/home/Sandbox/Docker#
```

then build 

an play on various commands ADD/COPY etc....

```
root@squadlabo:/home/Sandbox/Docker# docker build - < Dockerfile
Sending build context to Docker daemon 2.048 kB
Step 1/4 : FROM alpine:latest
 ---> 196d12cf6ab1
Step 2/4 : MAINTAINER dbriand@squad.fr
 ---> Using cache
 ---> b67d45c2fe33
Step 3/4 : RUN apk update && apk add wget && rm -rf /var/cache/apk/*
 ---> Running in 2defe0cd23e5
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.8/community/x86_64/APKINDEX.tar.gz
v3.8.1-38-g898a0bb28a [http://dl-cdn.alpinelinux.org/alpine/v3.8/main]
v3.8.1-35-ga062ffc9e8 [http://dl-cdn.alpinelinux.org/alpine/v3.8/community]
OK: 9539 distinct packages available
(1/1) Installing wget (1.19.5-r0)
Executing busybox-1.28.4-r1.trigger
OK: 5 MiB in 14 packages
 ---> 2713e22f3620
Removing intermediate container 2defe0cd23e5
Step 4/4 : ENTRYPOINT
 ---> Running in cd9ec3b4a55c
 ---> 1a9c2096382e
Removing intermediate container cd9ec3b4a55c
Successfully built 1a9c2096382e
root@squadlabo:/home/Sandbox/Docker#
root@squadlabo:/home/Sandbox/Docker#
root@squadlabo:/home/Sandbox/Docker#
root@squadlabo:/home/Sandbox/Docker#
root@squadlabo:/home/Sandbox/Docker#
root@squadlabo:/home/Sandbox/Docker# cat Dockerfile
FROM alpine:latest

MAINTAINER dbriand@squad.fr

RUN apk update && apk add wget && rm -rf /var/cache/apk/*

ENTRYPOINT [""]
root@squadlabo:/home/Sandbox/Docker#
```

presentation : docker registry 

presentation : docker networks

minimal network conf (buit at docker installation)

```
root@squadlabo:/home/Sandbox/Docker# ip a | grep -i docker
14: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default
    inet 172.17.0.1/16 scope global docker0
root@squadlabo:/home/Sandbox/Docker#
``` 

We are going to stop here for today 

Clean my tests & keep usefull containers

```
root@squadlabo:/home/Sandbox/Docker# docker images
REPOSITORY           TAG                 IMAGE ID            CREATED             SIZE
<none>               <none>              1a9c2096382e        24 minutes ago      4.89 MB
ubuntu               latest              ea4c82dcd15a        5 days ago          85.8 MB
nginx                latest              dbfc48660aeb        8 days ago          109 MB
debian               latest              be2868bebaba        8 days ago          101 MB
alpine               latest              196d12cf6ab1        6 weeks ago         4.41 MB
jdeathe/centos-ssh   latest              b92c8ffa02c7        2 months ago        222 MB
nginxdemos/hello     latest              aedf47d433f1        8 months ago        16.8 MB
perarneng/fortune    latest              e91c44650340        3 years ago         3.83 MB
root@squadlabo:/home/Sandbox/Docker#
root@squadlabo:/home/Sandbox/Docker#
root@squadlabo:/home/Sandbox/Docker#
root@squadlabo:/home/Sandbox/Docker# docker rmi 1a9
Deleted: sha256:1a9c2096382e818899d117a15ba944f5d9e59ed07c87c989391ac74200f51f47
Deleted: sha256:2713e22f36206d78c05028aa4810242123c115659bf4fdf31eebbae42faa69b5
Deleted: sha256:3c42190ef02cc7345321391b91fb7c9ef9b94640eb77abbb3852c3b20a236fc3
root@squadlabo:/home/Sandbox/Docker#
```


The support document will be added soon.



[link to personnal Dockerfile test](https://gitlab.com/squad-lcv/labo/blob/master/docs/docker/)

